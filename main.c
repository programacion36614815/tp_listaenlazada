#include <stdio.h>
#include "instancias.h"
#include "funciones.h"

int main() {

    int oper = 0;
    Lista* lista = crearLista();

    while(oper != 6) {
        printf("Ingrese el numero de la operacion a realizar \n");
        printf("1)Agregar Elemento a la lista \n");
        printf("2)Obtener largo de la lista \n");
        printf("3)Obtener un elemento de la lista \n");
        printf("4)Eliminar un elemento de la lista \n");
        printf("5)Imprimir la lista \n");
        printf("6)Salir \n");
        scanf("%d", &oper);
        switch (oper) {
            case 1:
                lista = agregarElemento(lista);
                break;
            case 2:
                obtenerLargo(lista);
                break;
            case 3:
                obtenerElemento(lista);
                break;
            case 4:
                lista = eliminarElemento(lista);
                break;
            case 5:
                imprimirLista(lista);
                break;
            case 6:
                printf("Adios \n");
                break;
            default:
                printf("Operacion invalida, vuelve a intentar \n");
        }
    }

    return 0;
}