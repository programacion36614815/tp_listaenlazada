//
// Created by joeld on 29/4/2023.
//

#ifndef EJERCICIO3B_FUNCIONES_H
#define EJERCICIO3B_FUNCIONES_H


#include <malloc.h>
#include "instancias.h"

Lista* agregarElementoLista(Lista *lista, int valor){
    Lista *nuevoNodo = malloc(sizeof(Lista));
    nuevoNodo->valor = valor;
    nuevoNodo->proximo = NULL;

    if (lista == NULL){
        lista = nuevoNodo;
    } else{
        Lista *cursor = lista;
        while (cursor->proximo != NULL){
            cursor = cursor->proximo;
        }
        cursor->proximo = nuevoNodo;
    }
    return lista;
}

Lista* agregarElemento(Lista *lista){

    Lista* lstNueva = NULL;
    int valor=0;
    int insert = 0;

    printf("------------------------------------------\n");

    printf("Ingrese el numero que quiere agregar a la lista\n");
    scanf("%d",&valor);

    while(lista != NULL){
        if(lista->valor < valor){
            lstNueva = agregarElementoLista(lstNueva, lista->valor);
            if (lista->proximo == NULL){
                lstNueva = agregarElementoLista(lstNueva, valor);
            }
        } else if(lista->valor > valor) {
            if(insert == 0){
                lstNueva = agregarElementoLista(lstNueva, valor);
            }
            lstNueva = agregarElementoLista(lstNueva, lista->valor);
            insert= 1;
        }
        lista = lista->proximo;
    }

    printf("El valor %d ha sido agregado a la lista\n",valor);
    printf("------------------------------------------\n");

    return lstNueva;
}

Lista* crearLista(){

    Lista *lista = NULL;
    int valor=0;

    printf("Creando lista\n");
    printf("Primer valor de la lista\n");

    printf("Ingrese el primer numero de la lista\n");
    scanf("%d",&valor);

    lista = agregarElementoLista(lista, valor);

    return lista;
}


void obtenerLargo(Lista *lista){

    int cant = 0;

    printf("------------------------------------------\n");

    while(lista != NULL){
        cant++;
        lista = lista->proximo;
    }

    printf("La cantidad de elementos en la lista es de: %d\n", cant);
    printf("------------------------------------------\n");
}

void obtenerElemento(Lista *lista){

    int valor=0;
    int ban= 0;
    int pos = 0;
    int posValor = 0;

    printf("------------------------------------------\n");

    printf("Ingrese el valor que quiere buscar: \n");
    scanf("%d",&valor);

    while(lista != NULL && ban == 0){
        if(lista->valor == valor){
            posValor = pos;
            ban = 1;
        }
        pos++;
        lista = lista->proximo;
    }

    if (ban == 1){
        printf("El valor %d se encuentra en la posicion: %d\n",valor, posValor);
    } else{
        printf("El valor no se encuentra en la lista\n");
    }

    printf("------------------------------------------\n");
}

void imprimirLista(Lista *lista){

    printf("------------------------------------------\n");
    int pos = 0;
    printf("LISTA:\n");
    while(lista != NULL){
        printf("Valor: %d, Posicion: %d\n",lista->valor, pos);
        pos++;
        lista = lista->proximo;
    }
    printf("------------------------------------------\n");
}

Lista* eliminarElemento(Lista *lista){

    int valor=0,ban = 0;
    Lista* lstNueva = NULL;

    printf("------------------------------------------\n");

    printf("Ingrese el numero que quiere eliminar de la lista: \n");
    scanf("%d",&valor);

    while(lista != NULL){
        if(lista->valor != valor){
            lstNueva = agregarElementoLista(lstNueva, lista->valor);
        } else {
            ban=1;
        }
        lista = lista->proximo;
    }

    if (ban == 1){
        printf("El valor %d a sido eliminado de la lista \n", valor);
        imprimirLista(lstNueva);
    } else{
        printf("El valor que desea eliminar no se encuentra en la lista\n");
        printf("------------------------------------------\n");
    }

    return lstNueva;

}

#endif //EJERCICIO3B_FUNCIONES_H
